---
title: "Final_Assignment"
author: "Casper Peters"
date: "7 juni 2017"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
###########################################################################
#                                                                         #
#   Re-estimating the reproduction number of                              #
#   Ebola virus disease (EVD) during the 2014-2016 outbreak               #
#   in West Africa using a complete dataset.                              #
#                                                                         #
#   THIS SCRIPT HAS BEEN WRITTEN BY:                                      #
#   CASPER PETERS                                                         #
#   DANNY ROOSSIEN                                                        #
#                                                                         #
#   https://bitbucket.org/Berghopper/bfv2_th8                             #  
#                                                                         #  
#   USING PARTS OF ALTHAUSES CODE:                                        #
#                                                                         #
#                                                                         #
#   REFERENCE: ALTHAUS CL. PLOS CURR. 2014;6.                             #
#                                                                         #
#   ESTIMATING THE REPRODUCTION NUMBER OF EBOLA VIRUS (EBOV)              #
#   DURING THE 2014 OUTBREAK IN WEST AFRICA                               #
#                                                                         #
#   DOI: 10.1371/currents.outbreaks.91afb5e0f279e7f29e7056095255b288      #
#                                                                         #
###########################################################################

rm(list=ls())
knitr::opts_chunk$set(echo = FALSE)
library(deSolve) # modeling package
library(lubridate) # date parsing package
library(bbmle)
library(knitr)
```

```{r functions}
#defining all functions needed 

# define function that describes the model to simulate
SEIRD <- function(t, state, params){
  with(as.list(c(state,params)),{
    # first calculate Bt
    Bt <- B*exp(-K*(t-T_))
    N <-S+E+I+R
    dS <- -Bt*S*I/N
    dE <- Bt*S*I/N-o*E
    dI <- o*E-y*I
    dR <- (1-f)*y*I
    dD <- f*y*I
    dC <- o * E
    
    # return list
    list(c(dS, dE, dI, dR, dD, dC))
  }
)}

# define function to find intersections between two vectors
# reference from stackoverflow: [https://stackoverflow.com/questions/20519431/finding-point-of-intersection-in-r]; Viewed on 13-06-2017
intersect_find <- function(x1, x2){
  # Find points where x1 is above x2.
  above<-x1>x2
  # Points always intersect when above=TRUE, then FALSE or reverse
  intersect.points<-which(diff(above)!=0)
  # Find the slopes for each line segment.
  x1.slopes<-x1[intersect.points+1]-x1[intersect.points]
  x2.slopes<-x2[intersect.points+1]-x2[intersect.points]
  # Find the intersection for each segment.
  x.points<-intersect.points + ((x2[intersect.points] - x1[intersect.points]) / (x1.slopes-x2.slopes))
  y.points<-x1[intersect.points] + (x1.slopes*(x.points-intersect.points))
  # return both
  return(c(round(x.points), y.points))
}

# make a function that combines simulation with real datapoints on dates and calculates some extras
combine_sim_exp <- function(simulated, real_data, merge.all = F){
  # fix all experiment dates into unix values
  real_data[,1] <- as_date(parse_date_time(real_data[,1], "ymd"))
  colnames(real_data) <- c("Date", "Cases", "Death")
  #merge dataset with simulation data, matching on date)
  combined <- merge(simulated, real_data, by.x='dates', by.y='Date', all.y=T, all=merge.all)

  return(combined)
}

# function that runs a simulation and parses the date correctly.
#input: simulation time, initial states, parameters, date outbreak start
#output: dataframe consisting of 2 columns: date and simulation results
do_sim <- function(times, y, parms, startdate){
  # first do our simulation
  the_sim <- ode(times = times, y = y, parms = parms, func = SEIRD)
  # make it into a dataframe so it's easier to plot
  the_sim_df <- data.frame(the_sim)
  #add the actual date to the dataframe
  the_sim_df$'dates' <- as_date(parse_date_time(startdate, "dmy"))+the_sim_df$time

  return(the_sim_df)
}

# transformation function for parameters 
trans <- function(pars) {
	pars["B"] <- exp(pars["B"])
	pars["K"] <- exp(pars["K"])
	pars["f"] <- plogis(pars["f"])
	pars["T_"] <- exp(pars["T_"])
	return(pars)
}

# negative loglikelihood function used for optimalization algorithm
# refrence from Github: [https://github.com/calthaus/Ebola/blob/master/West%20Africa%20%28PLOS%20Curr%202014%29/Ebola_outbreak_West_Africa_analysis.R] Viewed on 08-06-2017
nll <- function (B, f, K, o, y, T_, C_date, C_case, C_death) {
  # reconstruct country data
  country_data <- data.frame(C_date, C_case, C_death)
  
  #re-define params locally
  params<- c(B = B,
             f = f,
             K = K,
             o = o,
             y = y,
             T_ = T_
  )   
  # transform the parameters for calculation
  params <- trans(params)

  # calculate simulation
  sim_df <- do_sim(time, state, params, startdate)
  # merge it with experimental data
  merged_df <- combine_sim_exp(sim_df, country_data)
  
  #calculate how the simulation data fits the actual case counts
  total <- sum(dpois(merged_df$Cases, merged_df$C,log=T)) + sum(dpois(merged_df$Death, merged_df$D,log=T))
  return(-total)
  }

#an all-in function for estimating parameters
#input: free parameters, list of B/K/f/Tau1 initial values - B/K must be log transformed; fixed_params (o, y) and country dataframe
#output: returns a list of used parameter values that fits the outbreakdata most accurately
# refrence from Github: [https://github.com/calthaus/Ebola/blob/master/West%20Africa%20%28PLOS%20Curr%202014%29/Ebola_outbreak_West_Africa_analysis.R] Viewed on 08-06-2017
parameter_estimation <- function(free_params, fixed_params, country){
  results_fit <- mle2(nll,
                    start=as.list(free_params), 
                    fixed=fixed_params,
                    method="Nelder-Mead",
                    data=list(C_date=country$Date, C_case=country$Cases, C_death=country$Death)
                   )

  # return the final fitted result
  return(trans(coef(results_fit)))
}

# finally a function to plot the optimal simulation together with the experimental data
# also plots timecourse of Re and returns the intersection timepoint of Re
plot_optimsim_exp <- function(country_data, optim_params, main.plot){
  simulation <- do_sim(time, state, optim_params, startdate)
  # makes a plot with all deaths and cases
  matplot(x = simulation$dates, y = simulation[,6:7], type = 'l', 
          main=main.plot, xlab="", 
          ylab="Cumulative number of individuals", lty=c(1, 1), lwd=1.5,
          col=c("Black", "Red"), xaxt="n")
  # make xlab with mtext to move it down
  mtext("Dates", side=1, line=6)
  # make custom date labels
  # timerange each 1st of month
  timestamps <- sort(as_date(parse_date_time(paste0("01", sort(rep(month.abb[1:12], 4)), 2013:2016),"dmy")))
  axis(1,at=timestamps, labels=as.character(timestamps), las = 2)
  
  matplot(x = country_data$Date, y = country_data$Cases, type='p', pch=1, add = T, col = "Red")
  matplot(x = country_data$Date, y = country_data$Death, type='p', pch=0, add = T, col = "Black")
  # finish plot by adding legend
  labels <- c("simulation Deaths", "simulation Cases", "Experiment Cases", "Experiment Deaths")
  legend("topleft" , legend=labels, lty=c(1, 1, NA, NA), pch=c(NA, NA, 1, 1), lwd=1, col=c("Black", "Red", "Red", "Black"),
         cex=1)
  # now calculate and plot eventual Re
  N <- simulation$S+simulation$E+simulation$I+simulation$R
  y <- optim_params['y']
  K <- optim_params['K']
  B <- optim_params['B']
  T_ <- optim_params['T_']
  Bt <- Bt <- B*exp(-K*(simulation$time-T_))
  S <- simulation$S
  simulation$'Re' <- (Bt*S)/(y*N)
  # plot
  matplot(x = simulation$dates, y = simulation$Re, type = 'l', 
          main=main.plot, xlab="", 
          ylab="Effective reproduction number, Re", lty=1, lwd=1.5,
          col="Blue", xaxt="n")
  # make xlab with mtext to move it down
  mtext("Dates", side=1, line=6)
  # make custom date labels
  axis(1,at=timestamps, labels=as.character(timestamps), las = 2)
  
  # plot intersecting points
  # calculate intersections first
  R1 <- intersect_find(simulation$Re, rep(1 ,length(simulation$Re)))
  # plot them
  matplot(x = simulation$dates[R1[1]], y = R1[2], type='p', pch = 16, col = "Black", add = T)
  # add dashed lines
  # horizontal
  matplot(x = 1:simulation$dates[R1[1]], y = rep(R1[2], length(1:simulation$dates[R1[1]])), type='l', lty = 2, col = "Black", add = T)
  # vertical
  matplot(x = rep(simulation$dates[R1[1]], length(-1:R1[2])), y = -1:R1[2], type='l', lty = 2, col = "Black", add = T)
  
  # return date where Re is below 1
  return(as.character(simulation$dates[R1[1]]))
}
```

```{r data_parsing}
#read in dataset case counts
ebol_data <- read.csv("../Datasets/previous-case-counts.csv", header=T, sep=",", na.string="NA", stringsAsFactors=FALSE)
# fix colnames
colnames(ebol_data) <- c("Date", "Cases", "Death", "Cases", "Death", "Cases", "Death")
# fix all experiment dates into unix values
ebol_data[,1] <- as_date(parse_date_time(ebol_data[,1], "ymd"))
# split countries into different dataframes
Guinea <- na.omit(ebol_data[,1:3][-1,])
Sierra_Leone <- na.omit(ebol_data[,c(1,4,5)][-1,])
Liberia <- na.omit(ebol_data[,c(1,6,7)][-1,])
```

```{r fitting}
# predefine time and startdate
# daily, 900 days
time <- seq(0,900,1)

startdate <- "02 december 2013"

# predefine and guess fixed and free params
fixed <- c(T_ = -Inf, o = 1/5.3, y = 1/5.61) # assume that control interventions were already present. also, the average duration of the incubation and infectious period were fixed to previous estimates from an outbreak of the same EBOV subtype in Congo in 1995 (1/σ = 5.3 days and 1/γ = 5.61 days)

free <- c(B = log(0.2), K = log(0.001), f = 0) # B with log(0.2) gives an R0 of just above 1, k is assumed to matter only a little bit and f = 0 gives a fatality rate of 0.5 after logit transformation

# calculate most likely parameters for each country

# define state for each country depending on their population size
state <- c(S = (6.4532*10^6)-1,
           E = 0,
           I = 1,
           R = 0,
           D = 0,
           C = 1
)

params_Sierra_Leone <- parameter_estimation(free, fixed, Sierra_Leone)

state[1] <- (12.6086*10^6)-1

params_Guinea <- parameter_estimation(free, fixed, Guinea)

state[1] <- (4.5034*10^6)-1

params_Liberia <- parameter_estimation(free, fixed, Liberia)

```

```{r plotting, fig.height=9, fig.width=16, dpi=200, fig.cap="*Figure 1: Cumulative numbers of cases and deaths in experimental and simulation data of Guinea, Sierra Leone and Liberia plotted together with the effective reproduction number (Re) timecourse*"}
# simulate with most probable params and plot them together with experimental data
# this also plots Re plots to show the course of the reproduction number
par(mfrow=c(2,3), mar=c(6.1, 4.1, 4.1, 2.1), oma=c(1,0,0,0))
Re_intersect_date_Guin <- plot_optimsim_exp(Guinea, params_Guinea, "Guinea")
Re_intersect_date_Sier <- plot_optimsim_exp(Sierra_Leone, params_Sierra_Leone, "Sierra Leone")
Re_intersect_date_Libe <-plot_optimsim_exp(Liberia, params_Liberia, "Liberia")

# calculate R0's
R0s <- c(params_Guinea["B"]/params_Guinea["y"],
         params_Sierra_Leone["B"]/params_Sierra_Leone["y"],
         params_Liberia["B"]/params_Liberia["y"]
         )
# make vectors for each country
Guin <- c(R0s[1], params_Guinea[-4:-6], Re_intersect_date_Guin)
Sier <- c(R0s[2], params_Sierra_Leone[-4:-6], Re_intersect_date_Sier)
Libe <- c(R0s[3], params_Liberia[-4:-6], Re_intersect_date_Libe)

final_params <- matrix(c(Guin, Sier, Libe), ncol=3)
colnames(final_params) <- c("Guinea", "Sierra Leone", "Liberia")
rownames(final_params) <- c("Basic reproduction number, R0",
                            "Transmission rate β (per day)", 
                            "Case fatality rate, f", 
                            "Rate at which control measures reduce transmission, k (per day)",
                            "Date of Re declining below 1")
kable(final_params)
```













